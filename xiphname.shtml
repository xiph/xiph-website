﻿<!--#include virtual="/ssi/header.include" -->
<!--  Enter custom page information and styles here -->

<title>Xiph.org: naming</title>
<style type="text/css">
<!--

-->
</style>

</head>
<body>
<!--#include virtual="/ssi/xiphbar.include" -->
<!--#include virtual="/ssi/pagetop.include" -->
<!--  All your page content goes here  -->

<h1>What's in a name?</h1>

<h2>
	<a href="/">
		<img src="/images/logos/white-xifish.gif" alt="Xiph.Org"/>
		<img src="/images/logos/xiphword2.gif" alt=""/>
	</a>
</h2>

<p>
<a href="https://en.wikipedia.org/wiki/Green_swordtail"><i>Xiphophorus helleri</i></a> is a small aquarium fish (the common
Swordtail). What's special about it?  Not much, really.  The Xiph.Org
logo doesn't even look anything like a real swordtail, but it's a logo
that's been in use a long time.
</p>

<p>
What the name <em>does</em> have is the minimum requirement of one 
letter 'X' for a technology-related organization.  That fact that it's 
impossible to spell is an added bonus.
</p>

<p>
The Xiphophorus name was later shortened to Xiph.org. English speakers 
tend to pronounce this [zɪf] (short /i/) while non-English speakers 
favor [ksif]. Either is acceptable.
</p>


<h2>
	<a href="/ogg/">
		<img src="/images/logos/white-ogg.gif" alt="OggSquish"/>
		<img src="/images/logos/oggword2.gif" alt=""/>
	</a>
</h2>

<p>
The Ogg project began with a few-weekend-attempt at a simple audio
compression package as part of a larger project in 1993.  At the time,
the software was called 'Squish'.  The project and the general problem
of music compression became a personal fascination, and Squish took on
a life of its own far beyond the proportions of the original digital
music studio project of which it was to be part.
</p>

<p>
A few months after the first Squish webpage, I received a polite but
firm letter informing me that Squish is a registered trademark (for a
mail transport system).  Mike Whitson, a contributor to the cause in
the early days, suggested the name 'OggSquish' as a replacement.
</p>

<p>
An 'Ogg' (pronounced [ɑg], [ɒg], or [og]) is a tactical maneuver from
the network game 'Netrek' that has entered common usage in a wider
sense.  From the definition:</p>

<blockquote>
<p>3. To do anything forcefully, possibly without consideration of the
drain on future resources. "I guess I'd better go ogg the problem set
that's due tomorrow." "Whoops! I looked down at the map for a sec and
almost ogged that oncoming car."</p>
</blockquote>

<p>(see the
<a href="http://web.archive.org/web/20010222120446/info.astrian.net/jargon/terms/o/ogg.html">rest of the</a> 
<a href="http://www.catb.org/~esr/jargon/html/O/ogg.html">definition</a>
for the original Netrek usage.)
</p>

<p>
At the time Ogg was starting out, most personal computers were i386s
and the i486 was new.  I remember thinking about the algorithms I
was considering, "Woah, that's heavyweight.  People are going to need
a 486 to run that..."  While the software ogged the music, there
wasn't much processor left for anything else.
</p>

<p>
These days, Ogg has come to stand for the file format that developed 
from that early compression work and is part of the larger Xiph.org 
multimedia project; Squish became just the name of one of the 
Ogg codecs.
For that reason, we usually just refer to it as Ogg when there's no
Netrek context nearby.  The Ogg project has nothing to do with the
common surname 'Ogg'.  Nor is it named after 'Nanny Ogg' from the
Terry Pratchett book <cite>Wyrd Sisters</cite>.
</p>

<p>
The 'Thor-and-the-Snake' logo is drawn somewhat from Norse mythology;
the real symbolism is the sine-curve shape of the snake.  Thor is
hefting Mjollnir about to compress the periodic signal
J&ouml;rmungandr...  See, it all makes sense.
</p>

<h2>
	<a href="/vorbis/">
		<img src="/images/logos/white-ogg.gif" alt="Ogg Vorbis"/>
		<img src="/images/logos/vorbisword2.gif" alt=""/>
	</a>
</h2>

<p>
Vorbis, on the other hand <em>is</em> named after the Terry Pratchett
character from the book <cite>Small Gods</cite>. The name holds some
significance, but it's an indirect, uninteresting story.
</p>

<p>
<a href="/vorbis/">Ogg Vorbis</a> (pronouned [vōr'bĭs]) was the first CODEC in
developed as part of the Xiph.org multimedia project, begun
immediately after <a href="about/#fraunhofer">Fraunhofer issued
its 'Letter of Infringement' to freeware MP3 encoder efforts</a>.
Vorbis is intended to go head-to-head with MPEG codecs like AAC
and has historically achieved comparable or better quality.
</p>

<h2>
	<a href="/paranoia/">
		<img src="/images/logos/white-para.gif" alt="paranoia IV"/>
		<img src="/images/logos/paraword2.gif" alt=""/>
	</a>
</h2>

<p>
Paranoia IV is the upcoming release in the logical progression of
Paranoia, Paranoia II, Paranoia III...  Release IV is a cross platform
library project that combines a portable SCSI packet command
interfaces with platform-independant code to find specific hardware
devices.  On top of these it places specialized interfaces that wrap
the hardware in an error correcting layer to make up for deficiencies
in specific device examples. Paranoia IV provides the CDDA and error
correction engines to cdparanoia series 10.
</p>

<h2>
	<a href="/paranoia/">
		<img src="/images/logos/white-para.gif" alt="cdparanoia"/>
		<img src="/images/logos/cdparaword2.gif" alt=""/>
	</a>
</h2>

<p>
Cdparanoia is the error correcting compact disc digital audio
extraction (CDDA DAE) tool built using Paranoia III (currently, up to
release 9) and Paranoia IV (release 10, to be announced).
</p>

<p>
The name should be somewhat self-explanatory; the logo is a bit
weirder.  Dubbed 'the All-Seeing Laser Playback Head of Omniscience'
it's a takeoff of the eye-and-pyramid symbol of wisdom.  Think you've
seen it before and can't quite place where?  Look on the back of a US
one dollar bill.
</p>

<p>
An interesting note on the 'eye-in-the-pyramid' symbol from Nathan Myers:</p>
<blockquote>
<p>In the [this] page, you can explain the
"eye in the pyramid" symbol as indicating that which
exists solely because people believe it exists.
(Money and gods are examples, so it being on the
dollar bill is appropriate.)</p>
</blockquote>
<p>
The eye is placed on a starburst pattern emanating from the hub area
of a compact disc.</p>


<!--#include virtual="/ssi/pagebottom.include" -->
