<!--#include virtual="/ssi/header.include" -->
<!--  Enter custom page information and styles here -->
  <title>December 12, 2007: Xiph.Org Statement Regarding the HTML5 Draft and the Ogg Codec Set</title>
</head>
<body>
<!--#include virtual="/common/xiphbar.include" -->

<!--#include virtual="/ssi/pagetop.include" -->
<!--  All your page content goes here  -->

<h1>December 12, 2007: Xiph.Org Statement Regarding the HTML5 Draft and the Ogg Codec Set</h1>

<p>Ogg provides a baseline of fully unencumbered, fully open, fully
documented, fully royalty-free codecs that are lighter-weight than the
contemporary encumbered solutions while offering comparable or
superior performance. Ogg is not fantasy or vapourware. It is widely
deployed, tested, and reviewed. Ogg has already stood the test of time.</p>

<p>Xiph knows of no infringing technology in Ogg. Tens of millions of
copies have been deployed worldwide over the past ten years in a
diverse array of software and hardware products from small .orgs like
Wikimedia <a href="#1">[1]</a> to giant commercial vendors such as
Samsung <a href="#2">[2]</a> and Microsoft <a href="#3">[3]</a>.  Ogg
has triggered no litigation to date.</p>

<p>The same cannot be said for MPEG.  Despite the MPEG proponents'
claims that MPEG-licensed codecs protect against liability, patent
disputes involving MPEG codecs have occurred as recently as the past
few months. For example, Lucent v. Gateway <a href="#4">[4]</a> and
Multimedia Patent Trust v. Microsoft Corp. <a href="#5">[5]</a>.  The
MPEG-LA's own sublicense disclaimer warns that licensees are not
protected from patent-related litigation nor are they protected from
submarine patents. <a href="#6">[6]</a></p>

<p>The W3C has expressed a clear intention to officially define video
as an integral part of the web by introducing the &lt;video/&gt; tag.
Up to this point, video on the web has been presented primarily using
a fragmented array of proprietary extensions powered by encumbered
formats. Those who cannot use them have been made second-class
citizens. Failing to standardize on an unencumbered,
reasonably-performing format is a failure to advance beyond this
state.</p>

<p>In the interest of interoperability and to prevent large vendor
lock-in to proprietary methods, we support the W3C's desire
to adopt the unencumbered technology as the baseline.</p>

<p>Christopher "Monty" Montgomery [and others]
<br>Director
<br>Xiph.Org</p>

<ol>
<li><a href="http://commons.wikimedia.org/wiki/Commons:File_types">(Link to Wikimedia file types page)</a><p>

<li><a href="http://www.amazon.com/s?ie=UTF8&rh=n%3A172630%2Cp_4%3ASamsung%2Cp_supported_standards%3AOgg%20Vorbis&page=1">(Samsung Ogg players for sale at Amazon.com)</a><p>

<li>eg, Halo: <a href="http://www.postneo.com/2003/10/14/halo-for-pc-uses-ogg-vorbis">(Link to article about Ogg audio in Halo)</a>
<br>Specific features requested by the Halo team were added to the Vorbis
source tree in commits 4387, 4393, 4404, 4419 and 5222.<p>

<li><i>Lucent Technologies, Inc. V. Gateway, Inc.</i>, 2007 Wl 2900484
(S.D.Cal. Oct 01, 2007)<p>

<li><i>Multimedia Patent Trust v. Microsoft Corp.</i>, 2007 WL 2696675
(S.D.Cal.,2007)<p>

<li>"Moreover, The Mpeg LA sublicensee agreement explicitly warns that
the MPEG LA pool does not necessarily include all the patents
necessary to practice the technology and that sublicensee signs the
agreement aware of such risks." <br><i>Lucent Technologies, Inc. V. Gateway,
Inc.</i>, 2007 Wl 2900484 (S.D.Cal. Oct 01, 2007)<p>

</ol>


<!--#include virtual="/ssi/pagebottom.include" -->
